'use strict';
var util = require('util');
var path = require('path');
var yeoman = require('yeoman-generator');
var chalk = require('chalk');

var Zf5PlusGenerator = yeoman.generators.Base.extend({

	init: function () {
		this.pkg = require('../package.json');
		this.config.save();

		this.on('end', function () {
			if (!this.options['skip-install']) {
				this.installDependencies();
			}
		});
	},

	askForFontAwesome: function () {
		var cb = this.async();

		// have Yeoman greet the user.
		this.log(this.yeoman);

		this.log(chalk.bold.blue('=================='));
		this.log(chalk.bold.blue(' Yo Foundation 5!'));
		this.log(chalk.bold.blue('=================='));

		var prompts = {
			type: 'confirm',
			name: 'fontAwesome',
			message: 'Would you like to include Font Awesome? (Font Awesome gives you scalable vector icons..)',
			default: true
		};

		this.prompt(prompts, function (props) {
			this.fontAwesome = props.fontAwesome;

			cb();
		}.bind(this));
	},

	askForCompass: function () {
		var cb = this.async();

		var prompts = {
			type: 'confirm',
			name: 'compass',
			message: 'Would you like to use Scss (with Compass)? (default: Scss with Libsass)',
			default: false
		};

		this.prompt(prompts, function (props) {
			this.compass = props.compass;

			cb();
		}.bind(this));
	},

	askForJade: function () {
		var cb = this.async();

		var prompts = {
			type: 'confirm',
			name: 'jade',
			message: 'Would you like to use Jade? (templating engine)',
			default: false
		};

		this.prompt(prompts, function (props) {
			this.jade = props.jade;

			cb();
		}.bind(this));
	},

	askForOwlCarousel: function() {
	    var cb = this.async();

	    var prompts = {
	      type: 'confirm',
	      name: 'owlCarousel',
	      message: 'Would you like to use Owl Carousel version 1.3 fixed? (default: yes)',
	      default: true
	    };

	    this.prompt(prompts, function(props) {
	      this.owlCarousel = props.owlCarousel;

	      cb();
	    }.bind(this));
	  },

	  askForNiceScroll: function() {
	    var cb = this.async();

	    var prompts = {
	      type: 'confirm',
	      name: 'niceScroll',
	      message: 'Would you like to use Nice Scroll? (default: no)',
	      default: false
	    };

	    this.prompt(prompts, function(props) {
	      this.niceScroll = props.niceScroll;

	      cb();
	    }.bind(this));
	  },

	  askForFabric: function() {
	    var cb = this.async();

	    var prompts = {
	      type: 'confirm',
	      name: 'fabric',
	      message: 'Would you like to use FabricJS? (default: no)',
	      default: false
	    };

	    this.prompt(prompts, function(props) {
	      this.fabric = props.fabric;

	      cb();
	    }.bind(this));
	  },

	  askForThreejs: function() {
	    var cb = this.async();

	    var prompts = {
	      type: 'confirm',
	      name: 'threejs',
	      message: 'Would you like to use Threejs? (default: no)',
	      default: false
	    };

	    this.prompt(prompts, function(props) {
	      this.threejs = props.threejs;

	      cb();
	    }.bind(this));
	  },

	  askForSpritespin: function() {
	    var cb = this.async();

	    var prompts = {
	      type: 'confirm',
	      name: 'threejs',
	      message: 'Would you like to use Spritespin? (default: no)',
	      default: false
	    };

	    this.prompt(prompts, function(props) {
	      this.spritespin = props.threejs;

	      cb();
	    }.bind(this));
	  },

	  askForBourbon: function() {
	    var cb = this.async();

	    var prompts = {
	      type: 'confirm',
	      name: 'threejs',
	      message: 'Would you like to use Bourbon? (default: yes)',
	      default: true
	    };

	    this.prompt(prompts, function(props) {
	      this.bourbon = props.bourbon;

	      cb();
	    }.bind(this));
	  },


	  askForFitText: function() {
	    var cb = this.async();

	    var prompts = {
	      type: 'confirm',
	      name: 'fittext',
	      message: 'Would you like to use Fittext? (default: no)',
	      default: false
	    };

	    this.prompt(prompts, function(props) {
	      this.fittext = props.fittext;

	      cb();
	    }.bind(this));
	  },




	app: function () {
		this.mkdir('dist');
		this.mkdir('app');
		this.mkdir('app/assets');
		this.template('bower.json', 'bower.json');
		this.template('package.json', 'package.json');
		this.template('Gruntfile.js', 'Gruntfile.js');
		this.copy('.jshintrc', '.jshintrc');
		this.copy('.bowerrc', '.bowerrc');
		this.copy('gitignore', '.gitignore');
		this.copy('README.md', 'README.md');
		if (this.jade) {
			this.template('jade/index.jade', 'app/index.jade');
			this.template('jade/header.jade', 'app/header.jade');
			this.copy('jade/footer.jade', 'app/footer.jade');
		} else {
			this.template('index.html', 'app/index.html');
		}
		this.mkdir('app/assets/fonts');
		this.mkdir('app/assets/images');
		this.mkdir('app/assets/js');
		this.mkdir('app/assets/css');
		this.mkdir('app/assets/scss');
		this.copy('scss/app.scss', 'app/assets/scss/app.scss');
		this.copy('scss/_settings.scss', 'app/assets/scss/_settings.scss');
		this.template('scss/_appstyles.scss', 'app/assets/scss/_appstyles.scss');
		if(!this.bourbon){
			this.template('scss/_my-mixins.scss', 'app/assets/scss/_my-mixins.scss');
		}
		this.copy('js/app.js', 'app/assets/js/app.js');
		this.copy('css/template_override.css', 'app/assets/css/app_override.css');
	}

});

module.exports = Zf5PlusGenerator;
